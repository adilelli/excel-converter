const express = require('express');
const multer = require('multer');
const fs = require('fs');
const jwt = require('jsonwebtoken');
const verifyToken = require('./auth');
const excelToJson = require('./conversion');
const { saveJsonData, getJsonData, getAllJsonData } = require('./db');
require('dotenv').config();

// Initialize the Express app
const app = express();

// Secret key for JWT
const JWT_SECRET = process.env.JWT_SECRET;

// Set up Multer for file uploads
const upload = multer({ dest: 'uploads/' });

// Middleware to parse JSON and URL-encoded bodies
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// API endpoint to upload Excel file and get JSON response
// API endpoint to upload Excel file, convert to JSON, and save to MongoDB
app.post('/upload', verifyToken, upload.single('file'), async (req, res) => {
    const file = req.file;
    const sheetName = req.body.page;
    const name = req.body.name;

    if (!file) {
        return res.status(400).send('No file uploaded.');
    }

    if (!sheetName) {
        return res.status(400).send('Sheet name not provided.');
    }

    if (!name) {
        return res.status(400).send('Name not provided.');
    }

    try {
        // Convert the uploaded Excel file to JSON with the name header
        const jsonData = excelToJson(file.path, sheetName, name);

        // Save JSON data to MongoDB
        const savedData = await saveJsonData(jsonData);

        // Delete the uploaded file after conversion
        fs.unlinkSync(file.path);

        // Send the saved data response
        res.json({ id: savedData._id, data: savedData.data });
    } catch (error) {
        console.error('Error processing file:', error);
        res.status(500).send(`Error processing file: ${error.message}`);
    }
});


// API endpoint to retrieve all JSON data from MongoDB
app.get('/loadData', verifyToken, async (req, res) => {
    try {
        const data = await getAllJsonData();
        res.json(data);
    } catch (error) {
        console.error('Error retrieving data:', error);
        res.status(500).send(`Error retrieving data: ${error.message}`);
    }
});

// API endpoint to retrieve JSON data from MongoDB by ID
app.get('/loadData/:id', verifyToken, async (req, res) => {
    const id = req.params.id;

    try {
        const data = await getJsonData(id);
        if (!data) {
            return res.status(404).send('Data not found.');
        }
        res.json(data);
    } catch (error) {
        console.error('Error retrieving data:', error);
        res.status(500).send(`Error retrieving data: ${error.message}`);
    }
});


app.put('/login', (req, res) => {
    // For demonstration, we will use a hardcoded user id. In a real application, you would validate user credentials.
    const userId = req.body.username; // This should be the authenticated user's ID
    const password = req.body.password;
    if(userId != 'adil' || password != 'adil'){
        return res.status(400).send(`either username of password is wrong ${userId}, ${password}`);
    }
    const token = jwt.sign({ id: userId }, JWT_SECRET, { expiresIn: '1h' });

    res.json({ auth: true, token });
});


// Start the server
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
