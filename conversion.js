const xlsx = require('xlsx');

function excelToJson(excelFilePath, page, name) {
    const workbook = xlsx.readFile(excelFilePath);
    const pageName = workbook.SheetNames[page];
    const worksheet = workbook.Sheets[pageName];

    if (!worksheet) {
        throw new Error(`Sheet with name "${pageName}" not found`);
    }

    const jsonData = xlsx.utils.sheet_to_json(worksheet, { defval: null });

    // Add the name field as a header
    const result = {
        name: name,
        data: jsonData
    };

    return result;
}

module.exports = excelToJson;

